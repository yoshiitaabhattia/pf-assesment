<?php
use Carbon\Carbon;

class ScenarioCCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function ScenarioC(AcceptanceTester $I)
    {
      $I->amOnPage('/');
      $I->click('Find agent');
      $I->click('a.tiles_tile:nth-child(1)');
      $datetime = Carbon::now();
      $folder = "output/";
      $AgentInfoFile= fopen($folder."AgentInfo".$datetime.".txt", "w") or die("Unable to open this damn file!");
      $txt = $I->grabTextFrom('.bioinfo_personal');
      fwrite($AgentInfoFile, $txt);
      $txt = $I->grabTextFrom('div.pane_section:nth-child(2)');
      fwrite($AgentInfoFile, $txt);
      fclose($AgentInfoFile);
      $I->makeScreenshot('ScenarioCAgentInfo');
    }
}
