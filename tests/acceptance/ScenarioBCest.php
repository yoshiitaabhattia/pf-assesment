<?php


class ScenarioBCest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }

    // tests
    public function ScenarioB(AcceptanceTester $I)
    {
    $I->amOnPage('/');
    $I->click('Find agent');
    $I->checkOption('div.searchform_column:nth-child(4) > div:nth-child(1) > div:nth-child(1)','Hindi','English','Arabic');
    $I->wait(5);
    $I->checkOption('.dropdown_popup-opened > div:nth-child(5)','Arabic');
    $I->wait(5);
    $I->checkOption('.dropdown_popup-opened > div:nth-child(19)', 'English');
    $I->wait(5);
    $I->checkOption('.dropdown_popup-opened > div:nth-child(25)','Hindi');
    $I->click('Find');
    $TotalCount = $I->grabTextFrom('.title');
    $I->writeToFile('TotalCount', $TotalCount);
    $I->checkOption('div.searchform_column:nth-child(5) > div:nth-child(1) > div:nth-child(1)','Nationality');
    $I->wait(5);
    $I->checkOption('.dropdown_popup-opened > div:nth-child(36)','India');
    $I->waitForElementNotVisible('.loader1_spinner',1000);
    $TotalCountFiltered = $I->grabTextFrom('.title');
    $I->writeToFile('TotalCount', $TotalCountFiltered);
    }
}
