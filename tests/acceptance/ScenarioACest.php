<?php
use Carbon\Carbon;
class ScenarioACest
{
    public function _before(AcceptanceTester $I)
    {
    }

    public function _after(AcceptanceTester $I)
    {
    }



    public function ScenarioA(AcceptanceTester $I)

    {
      $header=['Location','Prices'];
      $I->amOnPage('/');
      $I->waitForText('Recommended properties in Qatar');
      $I->checkOption('.dropdown-heightfull','Rent');
      $I->checkOption('.dropdown_popup-opened > div:nth-child(2)','Buy');
      $I->wait(5);
      $I->checkOption('.searchproperty_type > div:nth-child(1) > div:nth-child(1)','Property type');
      $I->checkOption('.dropdown_popup-opened > div:nth-child(3)', 'Villa');
      $I->wait(5);
      $I->checkOption('div.searchproperty_column:nth-child(3) > div:nth-child(1)','Min. bed');
      $I->checkOption('div.searchproperty_column:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(5)','3 Bedrooms');
      $I->wait(5);
      $I->checkOption('div.searchproperty_column:nth-child(3) > div:nth-child(2) > div:nth-child(1)','Max. bed');
      $I->checkOption('div.searchproperty_column:nth-child(3) > div:nth-child(2) > div:nth-child(2) > div:nth-child(6)', '7 Bedrooms');
      $I->fillField('City or Community or Tower','The Pearl');
      $I->click('Find');
      $I->wait(5);
      $I->see('3 bedroom villas for sale in The Pearl');
      $I->checkOption('.dropdown-bordered > div:nth-child(1)', 'Featured');
      $I->wait(5);
      $I->checkOption('.dropdown_popup-opened > div:nth-child(4)', 'Price (high)');
      $datetime = Carbon::now();
      $folder = "output/";
      $ListingInfoFile= fopen($folder."ListingInfo".$datetime.".csv", "w") or die("Unable to open this damn file!");
      fputcsv($ListingInfoFile,$header);
      $prices = $I->grabMultiple(['class' => 'card_price']);
      $locations = $I->grabMultiple(['class' => 'card_location']);
      foreach($locations as $line=>$location){
        if(!isset($locations[$line])||!isset($prices[$line])){
          break;
        }
        fputcsv($ListingInfoFile, [$locations[$line],$prices[$line]]);
      }
      fclose($ListingInfoFile);
   }
 }
