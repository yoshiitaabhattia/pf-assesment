# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This repository contains the test answers for Property Finder Assesment

### How do I get set up? ###

Note : Scenario A needs a seperate config.
Please change url to : 'http://propertyfinder.qa' in /tests/acceptance.suite.yml for Scenario A

Installation for Codeception :
1. composer require codeception/codeception -dev    (once its successful, run the below)
2. php vendor/bin/codecept      (once its successful, run the below)
3. php vendor/bin/codecept bootstrap

Installation for Carbon :
1. composer require nesbot/carbon -dev

How to run codeception tests:

1. All tests :
php vendor/bin/codecept run acceptance --steps

2. Scenario A :
php vendor/bin/codecept run acceptance ScenarioA --steps

3. Scenario B :
php vendor/bin/codecept run acceptance ScenarioB --steps

4. Scenario C :
php vendor/bin/codecept run acceptance ScenarioC --steps


### Who do I talk to? ###

Yoshita Bhatia - yoshiitaabhattia@gmail.com
